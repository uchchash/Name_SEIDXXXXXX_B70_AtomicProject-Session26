-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 25, 2017 at 09:00 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b70`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_b70` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `atomic_project_b70`;

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

DROP TABLE IF EXISTS `book_title`;
CREATE TABLE `book_title` (
  `id` int(13) NOT NULL,
  `book_title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_trashed`) VALUES
(1, 'HIMU', 'HUMAYUN AHMED', 'NO'),
(5, 'Satkahan', 'Shamresh Mojumder', 'NO'),
(12, 'Autobiography of Bappy', 'Bappy', 'NO'),
(13, 'fdgdg', 'sdfsdf', 'NO'),
(14, 'Sfdsgt', 'dsrdrtdr', 'NO'),
(15, 'dhgfjhfhd', 'dsfser', '2017-09-24 20:52:34'),
(17, 'dsgfdg', 'sdtfdster', 'NO'),
(18, 'fghdfhfdhytr', 'cgfhygfuftuy', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

DROP TABLE IF EXISTS `hobbies`;
CREATE TABLE `hobbies` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hobbies` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_trashed`) VALUES
(1, 'fhgvhjvhjgjh', 'Eating, Photography', '2017-09-24 18:21:23'),
(2, 'Rahim', 'Eating, Riding', '2017-09-24 18:21:29'),
(3, 'fhfjhgb', 'Riding', 'NO'),
(4, 'fdssertwser', 'Photography', 'NO'),
(5, 'vgjhgjg', 'Eating', 'NO'),
(6, 'fhghf', 'Riding, Photography', 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

DROP TABLE IF EXISTS `profile_picture`;
CREATE TABLE `profile_picture` (
  `id` int(13) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_trashed` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'NO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_picture`, `is_trashed`) VALUES
(8, 'pic6', '1506310369gallery-img1.jpg', 'NO');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
